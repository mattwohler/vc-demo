<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

$this->get('/', function () {
    return view('welcome');
});

$this->group(['middleware' => 'auth'], function () {
    $this->get('/home', ['uses' => 'HomeController@index', 'as' => 'home']);

    $this->group(['prefix' => 'users'], function () {
        $this->post('send-vc', 'UsersController@sendVC');
        $this->get('notifications', 'UsersController@getNotifications');
        $this->patch('notifications/reset', 'UsersController@resetNotifications');
    });

    $this->group(['prefix' => 'logs'], function () {
        $this->get('transactions', ['uses' => 'LogTransactionsController@index', 'as' => 'logs.transactions']);
    });
});
