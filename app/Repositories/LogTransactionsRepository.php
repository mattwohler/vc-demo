<?php

namespace App\Repositories;

use App\User;
use App\UserToUserTransaction;

class LogTransactionsRepository
{
    /**
     * Query's the db for the Log Transactions data set.
     *
     * @param User $user
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function get(User $user)
    {
        return UserToUserTransaction::with('sender', 'receiver')
                ->where('sender_id', $user->id)
                ->orWhere('receiver_id', $user->id)
                ->orderByDesc('id')
                ->paginate(15);
    }
}
