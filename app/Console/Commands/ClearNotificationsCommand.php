<?php

namespace App\Console\Commands;

use App\Notification;
use Illuminate\Console\Command;

class ClearNotificationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:clear-notifications {--viewed-only}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all users notifications [--viewed-only]';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Notification $notification)
    {
        $this->info('Commence clearing user notifications...');

        $notifications = $notification->newQuery();

        if ($this->option('viewed-only')) {
            $notifications->where('viewed', true);
        }

        $notifications->delete();

        $this->info('User notifications have been cleared...');
    }
}
