<?php

namespace App\Console\Commands;

use App\User;
use App\Services\UserVcService;
use Illuminate\Console\Command;

class VCAccruementCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:vc-accruement {--amount=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'All users accrue VC amount [--amount=]';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(User $user)
    {
        $this->info('Commencing user accruement...');

        $amount = $this->option('amount') ?? config('demo.users.accrue_amount');

        $user = User::first();

        $service = new UserVcService($user);

        foreach ($user->all()->except($user->id) as $user) {
            $service->send((float) $amount)->to($user);
        }

        $this->info('User accruement complete...');
    }
}
