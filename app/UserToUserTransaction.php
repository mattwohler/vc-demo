<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserToUserTransaction extends Model
{
    /**
     * Get the Sending User involved in the Transaction.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    /**
     * Get the Receiving User involved in the Transaction.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }
}
