<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class SendVCRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*.user_id' => [
                'required',
                'integer',
                'exists:users,id',
                function ($attribute, $value, $fail) {
                    if ($value == $this->user()->id) {
                        $fail('Unable to send VC to yourself.');
                    }
                },
            ],
            '*.amount' => 'required|numeric',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.user_id.exists' => 'A :attribute does not exist.',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            '*.user_id' => 'user',
            '*.amount' => 'amount',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $userFromAmount = $this->user()->vc;
            $usersToAmount = array_sum($this->input('*.amount'));

            if ($userFromAmount < $usersToAmount) {
                $validator->errors()->add('0.user_id', 'You do not have enough VC to fulfill this request.');
            }
        });
    }
}
