<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\LogTransactionsRepository;

class LogTransactionsController extends Controller
{
    /**
     * Display the Log Transactions
     *
     * @param Request $request
     * @param LogTransactionsRepository $repository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, LogTransactionsRepository $repository)
    {
        return view('logs.transactions.index', [
            'transactions' => $repository->get(auth()->user()),
        ]);
    }
}
