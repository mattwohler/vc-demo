<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\SendVCRequest;

class UsersController extends Controller
{
    /**
     * Get Notifications for the given User.
     *
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNotifications(Request $request)
    {
        return $request->user()
                       ->notifications()
                       ->where('viewed', false)
                       ->get();
    }

    /**
     * Reset Notifications for the given User.
     *
     * @param int $userId
     * @return \App\User
     */
    public function resetNotifications(Request $request)
    {
        return tap($request->user(), function ($user) {
            $user->notifications()->update(['viewed' => true]);
        });
    }

    /**
     * Sends Virtual Currency to one or more Users.
     *
     * @param int $userId
     * @return \App\User
     */
    public function sendVC(SendVCRequest $request)
    {
        return tap($request->user())->sendVC(
            $request->all()
        );
    }
}
