<?php

namespace App\Services;

use App\User;
use App\Transaction;
use App\Events\BalanceUpdated;
use App\Events\IncomingTransfer;

class UserVcService
{

    /**
     * @var \App\User
     */
    protected $fromUser;

    /**
     * @var \App\User
     */
    protected $toUser;

    /**
     * @var float
     */
    protected $amount;

    /**
     * Create a new UserService instance.
     *
     * @return UserService
     */
    public function __construct(User $user = null)
    {
        $this->fromUser = $user;
    }

    /**
     * Fluently set the amount to send.
     *
     * @param float $amount
     * @return $this
     */
    public function send(float $amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Fluently set the from User.
     *
     * @param \App\User $user
     * @return $this
     */
    public function from(User $user)
    {
        $this->fromUser = $user;
    }

    public function to(User $user)
    {
        $transaction = Transaction::create([
            'amount' => $this->amount,
        ]);

        $user->addVC($transaction);

        $this->fromUser->subtractVC($transaction);

        $user->notifications()->create([
            'message' => sprintf('%s VC has been deposited into your account.', $this->amount),
        ]);

        BalanceUpdated::broadcast($user);
        BalanceUpdated::broadcast($this->fromUser);

        IncomingTransfer::broadcast($user);
    }
}
