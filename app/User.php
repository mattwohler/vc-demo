<?php

namespace App;

use App\Services\UserVcService;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'vc',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get all of the posts for the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    /**
     * Get the user that received the Transaction.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transactions()
    {
        return $this->belongsToMany(Transaction::class)->withPivot('type');
    }

    /**
     * Add Virtual Currency to the User
     *
     * @return void
     */
    public function addVC(Transaction $transaction)
    {
        $this->vc += $transaction->amount;
        $this->save();

        $this->attachTransaction($transaction, 'credit');
    }

    /**
     * Subtract Virtual Currency from the User Account
     *
     * @return void
     */
    public function subtractVC(Transaction $transaction)
    {
        $this->attachTransaction($transaction, 'debit');

        if ($this->isSystemUser()) {
            return;
        }

        $this->vc -= $transaction->amount;
        $this->save();
    }

    /**
     * Determines if User is a System Admin
     *
     * @return bool
     */
    public function isSystemUser()
    {
        return $this->id == 1;
    }


    /**
     * Attach a Transaction onto the User.
     *
     * @param Transaction $payload
     * @return void
     */
    protected function attachTransaction(Transaction $transaction, string $type)
    {
        $this->transactions()->save($transaction, compact('type'));
    }

    /**
     * Transfer Virtual Currency to an array of Users.
     *
     * @param array $payload
     * @return void
     */
    public function sendVC(array $payload)
    {
        $service = new UserVcService($this);

        foreach ($payload as $row) {
            $receivingUser = $this->find($row['user_id']);

            $service->send($row['amount'])->to($receivingUser);
        }
    }
}
