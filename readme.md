# Demo App

This app was built in part of my application as PHP Developer for Blockchain Intelligence Group.

### Installation

This app was built on top of Laravel's vagrant box (Homestead). Due to the usage of WebSockets, slight changes may be required to the `broadcasting.connections.pusher` configuration settings.

Assuming you're inside the VM or have the necessary dependencies, please run the following from within the project dir.,

```sh
$ cp .env.example .env
$ composer install
$ php artisan key:generate
$ php artisan migrate
$ php artisan db:seed
$ npm install
```

If you would like to WebSockets server to always be running, please follow these additional steps, otherwise you may simple run: `php artisan websockets:serve`.

```sh
$ sudo systemctl enable supervisor
$ sudo vim /etc/supervisor/conf.d/websockets.conf
```

Add the following contents to the file:

```sh
[program:websockets]
command=/usr/bin/php /path/to/application/artisan websockets:serve
numprocs=1
autostart=true
autorestart=true
user=vagrant
```

You may now start `supervior` by running the following commands:

```sh
$ sudo supervisorctl update
$ sudo supervisorctl start websockets

## Optionally verify that the server is running:
$ sudo supervisorctl status
```

Everything should be running properly. Try running the unit tests!

```sh
$ phpunit
$ php artisan dusk
```

### Troubleshooting

If the final dusk test fails, this means there's something wrong with the websockets. This only ever occurs after the first time you attempt to set it up. https://github.com/beyondcode/laravel-websockets/issues/24
