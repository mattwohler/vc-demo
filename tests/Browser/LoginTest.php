<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends DuskTestCase
{
    use DatabaseTransactions;

    public function test_user_can_login()
    {
        $this->browse(function ($browser) {
            $browser->visit('/login')
                    ->type('email', User::first()->email)
                    ->type('password', 'secret')
                    ->press('Login')
                    ->assertPathIs('/home');
        });
    }
}
