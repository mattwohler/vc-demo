<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Tests\Browser\Pages\DashboardPage;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends DuskTestCase
{
    use DatabaseTransactions;

    public function test_user_can_see_balance()
    {
        $user = User::find(2);

        $this->browse(function ($browser) use ($user) {
            $browser->loginAs($user)
                ->visit(new DashboardPage)
                ->assertSee(sprintf('Remaining Balance: %s', $user->vc));
        });
    }

    public function test_user_can_add_user()
    {
        $user = User::find(2);

        $this->browse(function ($browser) use ($user) {
            $browser->loginAs($user)
                ->visit(new DashboardPage)
                ->clickLink('Add User')
                ->waitFor('#user_id_0')
                ->assertSee('User ID:');
        });
    }

    public function test_user_send_and_receive_vc_with_notifications()
    {
        $sender = User::find(2);
        $receiver = User::find(3);

        $this->browse(function ($browser, $secondBrowser) use ($sender, $receiver) {
            $secondBrowser->loginAs($receiver)
                ->visit(new DashboardPage)
                ->assertSee($receiver->vc)
                ->click('#notificationsDropdown');

            $browser->loginAs($sender)
                ->visit(new DashboardPage)
                ->clickLink('Add User')
                ->waitFor('#user_id_0')
                ->type('#user_id_0', $receiver->id)
                ->type('#amount_0', 1)
                ->assertSee($sender->vc)
                ->press('Send')
                ->waitForDialog()
                ->acceptDialog()
                ->assertSee($sender->vc - 1);

            $secondBrowser->assertSee($receiver->vc + 1)
                ->pause(2000)
                ->assertSeeIn('@counter', '1');
        });
    }
}
