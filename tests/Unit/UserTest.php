<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\Transaction;
use App\Events\BalanceUpdated;
use App\Events\IncomingTransfer;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UnitTest extends TestCase
{
    use RefreshDatabase;

    public function test_add_vc_method()
    {
        factory(User::class, 2)->create(['vc' => 2]);
        $user = User::find(2);

        $user->addVC(Transaction::create(['amount' => 2]));

        $this->assertEquals($user->vc, 4);
        $this->assertEquals(
            $user->transactions->first()->pivot->type,
            'credit'
        );
    }

    public function test_add_subtract_method()
    {
        factory(User::class, 2)->create(['vc' => 4]);
        $user = User::find(2);

        $user->subtractVC(Transaction::create(['amount' => 2]));

        $this->assertEquals($user->vc, 2);
        $this->assertEquals(
            $user->transactions->first()->pivot->type,
            'debit'
        );
    }

    public function test_sendvc_method()
    {
        Event::fake();

        factory(User::class, 4)->create(['vc' => 10]);
        $sendingUser = User::find(2);

        $payload = [
            ['user_id' => 3, 'amount' => 2],
            ['user_id' => 4, 'amount' => 3],
        ];

        $sendingUser->sendVC($payload);

        $this->assertEquals($sendingUser->vc, 5);
        $this->assertEquals(User::find(3)->vc, 12);
        $this->assertEquals(User::find(4)->vc, 13);

        Event::assertDispatched(BalanceUpdated::class, 4);
        Event::assertDispatched(IncomingTransfer::class);
    }
}
