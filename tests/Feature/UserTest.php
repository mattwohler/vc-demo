<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var \Illuminate\Database\Eloquent\Collection
     */
    protected $users;

    public function setUp()
    {
        parent::setUp();

        $this->users = factory(User::class, 4)->create(['vc' => 10]);
    }

    public function test_user_cant_transfer_to_themselves()
    {
        $payload = [
            ['user_id' => 2, 'amount' => 2]
        ];

        $response = $this->actingAs($this->users[1])
                         ->json('POST', '/users/send-vc', $payload);

        $response->assertStatus(422);
    }

    public function test_user_cant_transfer_more_than_their_amount()
    {
        $payload = [
            ['user_id' => 3, 'amount' => 20]
        ];

        $response = $this->actingAs($this->users[1])
                         ->json('POST', '/users/send-vc', $payload);

        $response->assertStatus(422);
    }
}
