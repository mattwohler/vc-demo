<?php

namespace Deployer;

require 'recipe/laravel.php';

set('application', 'vc_demo');
set('repository', 'git@bitbucket.org:mattwohler/vc-demo.git');


set('allow_anonymous_stats', false);

// Hosts
host('vc-demo.com')
    ->hostname('vc-demo.com')
    ->stage('production')
    ->identityFile('~/.ssh/id_rsa')
    ->set('deploy_path', '{{release_path}}');

// Tasks
task('php-fpm:restart', function () {
    run('sudo /usr/sbin/service php7.2-fpm reload');
})->desc('Restart PHP-FPM service');

after('deploy', 'php-fpm:restart');
