<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateUserToUserTransactionsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->dropView();

        $this->createView();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->dropView();
    }

    /**
     * Drop the view using vanilla SQL.
     *
     * @return void
     */
    private function dropView()
    {
        DB::statement('DROP VIEW IF EXISTS user_to_user_transactions');
    }

    /**
     * Create the view using vanilla SQL.
     *
     * @return void
     */
    private function createView()
    {
        DB::statement("
            CREATE VIEW user_to_user_transactions AS
                SELECT
                t.id,
                t.amount,
                MAX( IF(tu.type = 'debit', u.id, null) ) as sender_id,
                MAX( IF(tu.type = 'credit', u.id, null) ) as receiver_id,
                t.created_at
                FROM transactions t
                LEFT JOIN transaction_user tu
                 ON tu.transaction_id = t.id
                LEFT JOIN users u
                 ON tu.user_id = u.id
                GROUP BY t.id
        ");
    }
}
