@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Remaining Balance:
                    <vc-balance
                        :init="{{ auth()->user()->vc }}"
                        :user_id="{{ auth()->user()->id }}"
                    ></vc-balance>
                </div>
            </div>
        </div>
    </div>

    <send-virtual-currency
        :user_id="{{ auth()->user()->id }}"
    ></send-virtual-currency>

</div>
@endsection
