@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Logs > Transactions</div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Sender</th>
                                    <th>Receiver</th>
                                    <th>Amount</th>
                                    <th class="text-right">Date/Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($transactions as $transaction)
                                    <tr>
                                        <td>{{ $transaction->id }}</td>
                                        <td>{{ $transaction->sender->name }}</td>
                                        <td>{{ $transaction->receiver->name }}</td>
                                        <td>{{ $transaction->amount }}</td>
                                        <td class="text-right" title="{{ $transaction->created_at }}">{{ $transaction->created_at }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card-footer">
                    <span style="float: right;">{{ $transactions->links() }}</span>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
